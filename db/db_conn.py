import sqlite3

class Database:
    def __init__(self, conn_data):
        try:
            self.conn = sqlite3.connect(conn_data)
            self.cursor = self.conn.cursor()
        except Error:
          print(Error)
        #finally:
        #  self.conn.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.connection.close()

    @property
    def connection(self):
        return self.conn

    def execute(self, sql, params=None):
        try:
            print(sql)
            print(params)
            self.cursor.execute(sql, params or ())
            self.conn.commit()
        except Error:
            print(Error)
            raise

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def commit(self):
        self.connection.commit()

    def query(self, sql, params=None):
        self.cursor.execute(sql, params or ())
        return self.fetchall()

    def query_one(self, sql, params):
        rows = self.cursor.execute(sql, params)
        return self.fetchone() if rows else None
