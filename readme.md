# Flask Restful API

## Descripción
Ejemplo de API restful en Python usando [Flask](https://www.palletsprojects.com/p/flask/). Se usa el modulo [Connexion](https://github.com/zalando/connexion) que crea dinamicamente las rutas a partir de un archivo de definicion YAML/JSON de [OpenAPI](https://www.openapis.org/).
Sobre esto se agrego Prance que permite dividir los archivos de definición, para no tener todas las entidades en uno solo

Tambien provee una interfaz [Swagger](https://swagger.io/) para poder probar la API sin necesidad de usar un cliente
La parte de autenticación no esta incluida por ahora en este ejemplo.
Para los datos se utiliza sqlite de modo que este ejemplo sea completamente funcional, pero cambiando solo algunas partes se pude hacer funcionar con MySQL / MariaDB y tal vez Postgres

## Como instalar

1. Clonar
2. Se recomienda usar un virtual enviroment, para crearlo y activarlo automaticamente correr el archivo `setup.cmd`
2. Correr `pip install -r requirements.txt`
3. Correr la aplicacion con `python server.py`

Por defecto corre en puerto 5000, pero se puede cambiar en el archivo server.py

Para acceder a la interfaz de Swagger: http://localhost:5000/api/ui/ (suponiendo que este corriendo en el puerto por defacto)

