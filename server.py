import connexion
import prance
from typing import Any, Dict
from pathlib import Path
from config import settings

#Esto nos permite usar varios YAML chicos en lugar de uno solo grande
def get_bundled_specs(main_file: Path) -> Dict[str, Any]:
    parser = prance.ResolvingParser(str(main_file.absolute()),
                                    lazy=True,
                                    backend='openapi-spec-validator')
    parser.parse()
    return parser.specification

# Crea instancia de aplicacion
app = connexion.App(__name__, specification_dir="./")

# Lee el archivo de swagger y crea los endpoints
app.add_api(get_bundled_specs(Path("config/resources/swagger.yaml")),
            resolver=connexion.RestyResolver("modules"))

# Crea la ruta raiz para la aplicacion (para "/")
@app.route('/')
def home():
    return ('Bienvenido a la API de Flask')


if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
