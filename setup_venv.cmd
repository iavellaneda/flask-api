@echo off
echo -- Configurando virtual environment --
IF EXIST %CD%\venv GOTO noaction
echo Creando carpeta
python -m venv venv
goto activate
:noaction
echo VENV ya existe
:activate
REM cd venv\scripts
call venv\scripts\activate
:end
